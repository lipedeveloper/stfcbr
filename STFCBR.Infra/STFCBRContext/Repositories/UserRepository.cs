﻿using MongoDB.Driver;
using STFCBR.Domain.Entities;
using STFCBR.Domain.Interfaces.Repositories;
using STFCBR.Infra.CommomContext.DataContext;
using STFCBR.Infra.CommomContext.Repositories;
using System.Linq;

namespace STFCBR.Infra.STFCBRContext.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    { 
        public UserRepository(STFCBRDataContext dataContext) : base("Users", dataContext) {
          
        }

        public User AccessLogin(string email, string password)
        {
            return collection.Find<User>(entity => entity.Email == email && entity.Password == password).ToList().FirstOrDefault();
        }
    }
}
