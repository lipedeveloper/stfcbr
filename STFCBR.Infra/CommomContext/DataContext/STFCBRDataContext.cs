﻿using MongoDB.Driver;
using STFC.CrossCutting.Configuration;

namespace STFCBR.Infra.CommomContext.DataContext
{
    public class STFCBRDataContext
    {
        public STFCBRDataContext(AppSettings appSettings) => Database = new MongoClient(appSettings.MongoConfig.ConnectionString).GetDatabase(appSettings.MongoConfig.Name);

        public IMongoDatabase Database { get; private set; }
    }
}
