﻿using MongoDB.Bson;
using MongoDB.Driver;
using STFCBR.Domain.Entities;
using STFCBR.Domain.Interfaces.Repositories;
using STFCBR.Infra.CommomContext.DataContext;
using System;

namespace STFCBR.Infra.CommomContext.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity<ObjectId>
	{
		protected readonly STFCBRDataContext dataContext;
		protected readonly IMongoCollection<TEntity> collection;

		public Repository(string collectionName, STFCBRDataContext dataContext)
		{
			this.collection = dataContext.Database.GetCollection<TEntity>(collectionName);
			this.dataContext = dataContext;
		}

		public TEntity Add(TEntity entity)
		{
			try
			{
				collection.InsertOne(entity);
			}
			catch (Exception exception)
			{
			}

			return entity;
		}

		public void Remove(TEntity entity)
		{
			try
			{
				collection.DeleteOne(collectionEntity => collectionEntity.Id == entity.Id);
			}
			catch (Exception exception)
			{
			 
			}
		}

		public bool Exists(params object[] ids)
		{
			bool result = false;

			try
			{
				result = collection.Find<TEntity>(entity => entity.Id == (ObjectId)ids[0]).Any();
			}
			catch (Exception exception)
			{
			 
			}

			return result;
		}

		public TEntity Get(params object[] ids)
		{
			TEntity result = null;

			try
			{
				result = collection.Find<TEntity>(entity => entity.Id == (ObjectId)ids[0]).FirstOrDefault();
			}
			catch (Exception exception)
			{
				 
			}

			return result;
		}
 
		 
		public void Update(TEntity entity)
		{
			try
			{
				collection.ReplaceOne(collectionEntity => collectionEntity.Id == entity.Id, entity);
			}
			catch (Exception exception)
			{
				 
			}
		}

		public void Dispose() { }

 
	}
}
