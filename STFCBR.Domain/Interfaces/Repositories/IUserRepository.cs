﻿using STFCBR.Domain.Entities;

namespace STFCBR.Domain.Interfaces.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        public User AccessLogin(string email, string password);
    }
}
