﻿using System;

namespace STFCBR.Domain.Interfaces.Repositories
{
    public interface IRepository<TEntity> : IDisposable where TEntity : IEntity
    {
        TEntity Add(TEntity entity);
        bool Exists(params object[] id);
        TEntity Get(params object[] id);
        void Remove(TEntity entity);
        void Update(TEntity entity);
    }
}
