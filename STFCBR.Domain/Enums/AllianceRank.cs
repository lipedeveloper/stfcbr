﻿namespace STFCBR.Domain.Enums
{
    public enum AllianceRank
    {
        WithoutRank = 0,
        Agent = 1,
        Operative = 2,
        Premier = 3,
        Commodore = 4,
        Admiral= 5
    }
}
