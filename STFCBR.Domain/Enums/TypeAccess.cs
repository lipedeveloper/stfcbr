﻿namespace STFCBR.Domain.Enums
{
    public enum TypeAccess
    {
        Anonymous = 0,
        User = 1,
        Moderator = 2,
        Admin = 3,
        SuperAdmin = 4 
    }
}
