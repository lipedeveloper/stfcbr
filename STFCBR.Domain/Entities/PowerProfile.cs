﻿using System;

namespace STFCBR.Domain.Entities
{
    public class PowerProfile
    {
        public decimal Power { get; set; }
        public decimal PowerDestroyed { get; set; }
        public DateTime RegisterDate { get; set; }
        public DateTime LastRegisterDate { get; set; }
    }
}
