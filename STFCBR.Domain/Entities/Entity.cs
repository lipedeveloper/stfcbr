﻿using STFCBR.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;

namespace STFCBR.Domain.Entities
{
    public abstract class Entity<TIdType> : IEntity
	{
		public Entity() => Id = default(TIdType);

		public TIdType Id { get; protected set; }

		public override bool Equals(object obj)
		{
			bool result = false;
			Type objType = obj.GetType();

			if (this.GetType() == objType || objType.IsInstanceOfType(this.GetType()))
				result = Id.Equals(((Entity<TIdType>)obj).Id);

			return result;
		}

		public override int GetHashCode() => 2108858624 + EqualityComparer<TIdType>.Default.GetHashCode(Id);
	}
}
