﻿using STFCBR.Domain.Enums;

namespace STFCBR.Domain.Entities
{
    public class Alliances
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public AllianceRank Rank { get; set; }
    }
}
