﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STFCBR.Domain.Entities
{
    public class PlayerProfile
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public PowerProfile PowerProfile { get; set; }
    }
}
