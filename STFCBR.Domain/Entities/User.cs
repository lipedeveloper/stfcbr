﻿using MongoDB.Bson;
using STFCBR.Domain.Enums;

namespace STFCBR.Domain.Entities
{
    public class User : Entity<ObjectId>
    {
        public string RealName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string CelPhone { get; set; }
        public Alliances Alliances { get; set; }
        public PlayerProfile PlayerProfile { get; set; }
        public TypeAccess TypeAccess { get; set; }


        public User(string realName, string email, string password)
        {
            RealName = realName;
            Email = email;
            Password = password;
            TypeAccess = TypeAccess.User;
        }

        public User(string realName, string email, string password, Alliances alliances, PlayerProfile playerProfile, string celphone)
        {
            RealName = realName;
            Email = email;
            Password = password;
            Alliances = alliances;
            PlayerProfile = playerProfile;
            CelPhone = celphone;
            TypeAccess = TypeAccess.User;
        }
    }
}
