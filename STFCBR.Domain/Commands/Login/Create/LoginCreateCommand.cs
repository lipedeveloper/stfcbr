﻿using MediatR;
using STFCBR.Domain.Entities;

namespace STFCBR.Domain.Commands.Login.Create
{
    public class LoginCreateCommand : IRequest<LoginCreateCommandResponse>
    {
        public string RealName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Celphone { get; set; }

        public Alliances Alliances { get; set; }
        public PlayerProfile PlayerProfile { get; set; }
    }
}
