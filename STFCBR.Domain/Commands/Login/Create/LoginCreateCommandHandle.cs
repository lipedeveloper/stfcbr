﻿using MediatR;
using STFCBR.Domain.Entities;
using STFCBR.Domain.Interfaces.Repositories;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace STFCBR.Domain.Commands.Login.Create
{
    public class LoginCreateCommandHandle : IRequestHandler<LoginCreateCommand, LoginCreateCommandResponse>
    {
        IUserRepository _userRepository;
        public LoginCreateCommandHandle(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<LoginCreateCommandResponse> Handle(LoginCreateCommand request, CancellationToken cancellationToken)
        {
            var result = _userRepository.Add(new User(request.RealName, request.Email, Convert.ToBase64String(Encoding.ASCII.GetBytes(request.Password)), request.Alliances, request.PlayerProfile,request.Celphone));
            return new LoginCreateCommandResponse() { Response = result };
        }
    }
}
