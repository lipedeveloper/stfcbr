﻿using MediatR;
using STFCBR.Domain.Interfaces.Repositories;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace STFCBR.Domain.Commands.Login.Access
{
    public class LoginAccessAllCommandHandle : IRequestHandler<LoginAccessAllCommand, LoginAccessAllCommandResponse>
    {
        IUserRepository _userRepository;
        public LoginAccessAllCommandHandle(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }


        public async Task<LoginAccessAllCommandResponse> Handle(LoginAccessAllCommand request, CancellationToken cancellationToken)
        {
            if (!request.ValidLogin())
            {
                return new LoginAccessAllCommandResponse() { Success = false, Message = request.message };
            }

            var user = _userRepository.AccessLogin(request.email, Convert.ToBase64String(Encoding.ASCII.GetBytes(request.password)));
            var logged = user != null;
            if(user == null)
            {
                return new LoginAccessAllCommandResponse() { Success = logged, Message = "Email ou Senha inválido!" };
            }
            return new LoginAccessAllCommandResponse() { Success = logged, Message = !logged ? "Email ou Senha inválido!" : string.Empty, Data = new { realName = user.RealName, playerName = user.PlayerProfile.Name  } };
        }
    }
}
