﻿using MediatR;

namespace STFCBR.Domain.Commands.Login.Access
{
    public class LoginAccessAllCommand : IRequest<LoginAccessAllCommandResponse>
    {
        public string email { get; set; }
        public string password { get; set; }

        internal string message { get; set; }

        public bool ValidLogin()
        {
            if (string.IsNullOrEmpty(this.email) || string.IsNullOrEmpty(this.password))
            {
                this.message = "Preencha Email e Senha.";
                return false;
            }

            return true;
        }
    }
}
