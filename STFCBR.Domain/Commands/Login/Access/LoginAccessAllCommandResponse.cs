﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STFCBR.Domain.Commands.Login.Access
{
    public class LoginAccessAllCommandResponse
    {
        public string Token { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }

    }
}
