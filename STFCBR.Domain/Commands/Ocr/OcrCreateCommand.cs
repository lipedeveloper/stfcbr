﻿using MediatR;

namespace STFCBR.Domain.Commands.Ocr
{
    public class OcrCreateCommand : IRequest<OcrCreateCommandResponse>
    {
        public string UrlLink { get; set; }
    }
}
