﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STFCBR.Domain.Commands.Ocr
{
    public class OcrCreateCommandResponse
    {
        public object Result { get; set; }
    }
}
