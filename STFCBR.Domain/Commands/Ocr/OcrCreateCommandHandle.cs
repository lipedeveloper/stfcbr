﻿using IronOcr;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace STFCBR.Domain.Commands.Ocr
{
    public class OcrCreateCommandHandle : IRequestHandler<OcrCreateCommand, OcrCreateCommandResponse>
    {
        public async Task<OcrCreateCommandResponse> Handle(OcrCreateCommand request, CancellationToken cancellationToken)
        {
            string r;
            var Ocr = new IronTesseract(); // nothing to configure
            Ocr.Configuration.BlackListCharacters = "~`$#^*_}{][|\\@¢©«»°±·×‑–—‘’“”•…′″€™←↑→↓↔⇄⇒∅∼≅≈≠≤≥≪≫⌁⌘○◔◑◕●☐☑☒☕☮☯☺♡⚓✓✰";

            Ocr.Configuration.ReadBarCodes = false;

            using (var Input = new OcrInput(@"images\chart3.png"))
            {
                var Result = Ocr.Read(Input);
                r = JsonConvert.SerializeObject(Result);
                Console.WriteLine(Result.Text);
            }

            return new OcrCreateCommandResponse() { Result = r };
        }
    }
}
