﻿namespace STFC.CrossCutting.Configuration.AppSettingsModels
{
    public sealed class ApplicationConfig
    {
        public string AppName { get; set; }
        public string AppDescription { get; set; }
        public string AppVersion { get; set; }
    }
}
