﻿namespace STFC.CrossCutting.Configuration.AppSettingsModels
{
    public sealed class MongoConfig
    {
        public string ConnectionString { get; set; }
        public string Name { get; set; }
    }
}
