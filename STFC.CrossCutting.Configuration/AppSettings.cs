﻿using STFC.CrossCutting.Configuration.AppSettingsModels;

namespace STFC.CrossCutting.Configuration
{
    public sealed class AppSettings
    {
        public ApplicationConfig ApplicationConfig { get; set; }
        public MongoConfig MongoConfig { get; set; }
    }
}
