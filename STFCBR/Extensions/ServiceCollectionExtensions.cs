﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using STFC.CrossCutting.Configuration;
using STFCBR.Domain.Commands.Login.Access;
using STFCBR.Domain.Commands.Login.Create;
using STFCBR.Domain.Interfaces.Repositories;
using STFCBR.Infra.CommomContext.DataContext;
using STFCBR.Infra.STFCBRContext.Repositories;
using System;

namespace STFCBR.API.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSwagger(this IServiceCollection serviceCollection, AppSettings appSettings)
        {
            serviceCollection.AddSwaggerGen(x =>
            {
                x.SwaggerDoc(
                    "v1",
                    new OpenApiInfo()
                    {
                        Title = appSettings.ApplicationConfig.AppName,
                        Version = appSettings.ApplicationConfig.AppVersion,
                        Description = appSettings.ApplicationConfig.AppDescription,
                        Contact = new OpenApiContact
                        {
                            Name = "STFCBR",
                            Email = "felipearmentano@hotmail.com"
                        }
                    }

                    );
            });
            return serviceCollection;
        }

        public static IServiceCollection AddIoC(this IServiceCollection serviceCollection, AppSettings appSettings)
        {
            serviceCollection.AddScoped<STFCBRDataContext>(x => new STFCBRDataContext(appSettings));

            serviceCollection.AddMediatR(RegisterMediatR());
            serviceCollection.AddTransient<IUserRepository, UserRepository>();




            return serviceCollection;
        }


        internal static Type[] RegisterMediatR()
        {
            return new Type[] { typeof(LoginCreateCommand), typeof(LoginAccessAllCommand) };
        }

    }
}
