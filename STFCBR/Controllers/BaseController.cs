﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace STFCBR.API.Controllers
{
    public abstract class BaseController<T> : ControllerBase
    {
        protected readonly IMediator _mediator;

        protected BaseController(IMediator mediator)
        {
            _mediator = mediator;
        }
     
        protected async Task<IActionResult> CreateResponse<T>(Func<Task<T>> function)
        {
            try
            {
                var data = await function();
                return Ok(data);
            }
            catch (Refit.ApiException exApi)
            {
                return StatusCode((int)exApi.StatusCode, exApi.Content);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }

        }


    }
}
