﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using STFCBR.Domain.Commands.Ocr;
using System.Threading.Tasks;


namespace STFCBR.API.Controllers
{
    public class OcrController : BaseController<OcrController>
    {
        public OcrController(IMediator mediator) : base(mediator) { }


        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> ListPrograms([FromBody] OcrCreateCommand command) => await CreateResponse(async () => await _mediator.Send(command));

    }
}
