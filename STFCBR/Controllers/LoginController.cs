﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using STFCBR.API.Controllers;
using STFCBR.Domain.Commands.Login.Access;
using STFCBR.Domain.Commands.Login.Create;
using System.Threading.Tasks;

namespace STFCBR.Controllers
{
    [ApiController]
    [Route("api/Login")]
    public class LoginController : BaseController<LoginController>
    {
        public LoginController(IMediator mediator) : base(mediator) { }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> CreateUser([FromBody] LoginCreateCommand command) => await CreateResponse(async () => await _mediator.Send(command));

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> LogIn([FromBody] LoginAccessAllCommand command) => await CreateResponse(async () => await _mediator.Send(command));


    }
}
